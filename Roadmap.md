# CTG Bot Roadmap


## Liga

Um die CTG Liga verbessern zu können, soll folgendes möglich sein:
1. Sich als Spieler für die Season registrieren
2. Spieler möglichst automatisch den Divisionen zuweisen anhand von bisherigen Liga Ergebnissen, Elo und PB
3. Spieler sollen in der Lage sein, bisher gespielte Matches anzeigen zu lassen
4. Spieler sollen in der Lage sein, noch nicht gespielte Matches anzeigen zu lassen
5. Spieler sollen in der Lage sein, Spiele zu schedulen. Hierbei sollen die möglichen Restreamer gepingt werden und diese sollen einen Haken setzen können, um anzuzeigen, dass sie das Match restreamen können.
6. [TBA]

Außerhalb der Liga:

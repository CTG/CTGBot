import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Bot {

    //enter token
    String token = "token";

    public Bot() throws FileNotFoundException {
        init();
    }

    private void init() {
        JDA jda = JDABuilder.createDefault(token).enableIntents(GatewayIntent.MESSAGE_CONTENT).build();
        jda.addEventListener(new DiscordListener());
    }
}

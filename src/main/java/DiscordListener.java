import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class DiscordListener extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if (event.getAuthor().isBot()) return;

        Message message = event.getMessage();
        MessageChannel channel = event.getChannel();
        String parts[] = message.getContentRaw().split("?");

        cases(parts[1], channel);
    }

    private void cases(String command, MessageChannel channel){
        String parts[] = command.split("\\s");
        switch (parts[0]) {

            //CTG Liga commands

            case "register":
                break;
            case "unplayed":
                break;
            case "played":
                break;
            case "schedule":
                break;
            case "report":
                break;
            case "accept":
                break;
        }
    }
}
